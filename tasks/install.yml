---
# Install tasks for CertManager

- name: Create temp directory
  file:
    path: "{{ install_dir }}/certmanager"
    state: directory

- name: Create Namespace
  include_role:
    name: common/namespace
    tasks_from: namespace.yml
  vars:
    namespace: "{{ config.certmanager.namespace }}"

- name: Include k8s resource vars from "{{ config.certmanager.k8_resources_file }}"
  include_vars:
    file: "{{ config.certmanager.k8_resources_file }}"
  when: config.certmanager.k8_resources_file is defined and config.certmanager.k8_resources_file

- name: Configure Registry
  set_fact: 
    controller_image_repository: "{{ platform.harbor_registry }}/quay-proxy/jetstack/cert-manager-controller"
    webhook_image_repository: "{{ platform.harbor_registry }}/quay-proxy/jetstack/cert-manager-webhook"
    cainjector_image_repository: "{{ platform.harbor_registry }}/quay-proxy/jetstack/cert-manager-cainjector"
    ctl_image_repository: "{{ platform.harbor_registry }}/quay-proxy/jetstack/cert-manager-ctl"
  when: platform.harbor_registry is defined 

- name: Basic chart values
  set_fact: 
    chartvalues: 
      image:
        repository: "{{ controller_image_repository | default('quay.io/jetstack/cert-manager-controller') }}"
      installCRDs: true
      global:
        leaderElection:
          namespace: "{{ config.certmanager.namespace }}"
      resources: "{{ config.certmanager.certmanager_resources.certmanager |default(omit) }}"
      cainjector:
        image:
          repository: "{{ cainjector_image_repository | default('quay.io/jetstack/cert-manager-cainjector') }}"
        resources: "{{ config.certmanager.certmanager_resources.cainjector |default(omit) }}"
      webhook:
        image:
          repository: "{{ webhook_image_repository | default('quay.io/jetstack/cert-manager-webhook') }}"
        resources: "{{ config.certmanager.certmanager_resources.webhook |default(omit) }}"
      prometheus:
        servicemonitor:
          enabled: true
          labels:
            release: prometheus-operator
      startupapicheck:
        image:
          repository: "{{ ctl_image_repository | default('quay.io/jetstack/cert-manager-ctl') }}"

- name: Configure proxy server in env variables 
  set_fact: 
    chartvalues: "{{ chartvalues | combine(proxy_env) }}"
  vars: 
    proxy_env: 
      extraEnv: 
        - name: HTTP_PROXY
          value: "{{ cloudinfra.internet_proxy.HTTP_PROXY }}"
        - name: HTTPS_PROXY
          value: "{{ cloudinfra.internet_proxy.HTTPS_PROXY }}"
        - name: NO_PROXY
          value: "{{ cloudinfra.internet_proxy.NO_PROXY }}" 
  when: cloudinfra.internet_proxy is defined 

- name: Configure proxy certificate in Configmap on volumeMounts
  block: 
    - name: Create secret for proxy trust
      k8s:
        definition: "{{ lookup('template', 'proxy-ca-secret.yml') }}"

    - name: Configure certificate in Secret on volumeMounts
      set_fact: 
        chartvalues: "{{ chartvalues | combine(proxy_cert) }}"
      vars: 
        proxy_cert: 
          volumes:
            - name: proxy-cert
              secret:
                secretName: proxy-cert
          volumeMounts:
            - name: proxy-cert
              mountPath: /etc/ssl/certs
  when: cloudinfra.internet_proxy.certificates is defined 
    
- name: Install DoH Proxy (when using split DNS)
  block:
    - k8s:
        state: present
        wait: true
        definition: "{{ lookup('template', item) }}"
      loop:
        - "doh-configmap.yml"
        - "doh-deployment.yml"
        - "doh-service.yml"

    - name: Use DoH
      set_fact: 
        chartvalues: "{{ chartvalues | combine(extra_args) }}"
      vars: 
        extra_args:
          extraArgs: 
            - --dns01-recursive-nameservers-only
            - --dns01-recursive-nameservers=doh-proxy:53
  when: config.certmanager.use_dns_over_http | default(true)

- name: Install certmanager
  kubernetes.core.helm:
    release_name: cert-manager
    release_state: present
    release_namespace: "{{ config.certmanager.namespace }}"
    chart_ref: "{{ charts.certmanager.name }}"
    chart_repo_url: "{{ charts.certmanager.repo }}"
    chart_version: "{{ charts.certmanager.version }}"
    release_values: "{{ chartvalues }}"
    wait: true
